hotelApp.controller('MapsController', ['$scope', '$ionicLoading', '$compile', function($scope, $ionicLoading, $compile) {
    
    function initialize() {
        var dionPalace = new google.maps.LatLng(40.1500366, 22.5463697);

        var mapOptions = {
            streetViewControl:true,
            center: dionPalace,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);
        
        //Marker + infowindow + angularjs compiled ng-click
        var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";
        
        var compiled = $compile(contentString)($scope);

        var infowindow = new google.maps.InfoWindow({
            content: compiled[0]
        });

        $scope.marker = new google.maps.Marker({
            position: dionPalace,
            map: map,
            title: 'Dion Palace'
        });
   
        google.maps.event.addListener($scope.marker, 'click', function() {
            infowindow.open(map,$scope.marker);
        });
        
        $scope.map = map;
    }

    $scope.centerOnMe = function() {
        document.getElementById("map").style.height = "70%";
        document.getElementById("directions-panel-scroll").style.height = "30%";
        if(!$scope.map) {
            return;
        }

        navigation = navigator.geolocation.watchPosition(success, error, options);    
        navigator.geolocation.getCurrentPosition(function(pos) {
            initialize();
            $scope.marker.setMap(null);
            var locationPoint = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            var dionPoint = new google.maps.LatLng(40.1500366, 22.5463697);
        
            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer();

            var request = {
                origin : locationPoint,
                destination : dionPoint,
                travelMode : google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setPanel(document.getElementById('directions-panel'));
                }
            });

            directionsDisplay.setMap($scope.map);        

            $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
            $ionicLoading.hide();        
        
        
        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
        

    };
    
    
    var first=true;
    function geoChange(lat,lon) {

        if(first){
            first = false;
        }
        else{
            $scope.marker2.setMap(null);
        }
        
        
        $scope.marker2 = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 7
            },
            draggable: false,
            map: $scope.map
        });
        
        
        $scope.map.panTo($scope.marker2.position);
        $scope.map.setZoom(15);
        
    };    
      
    $scope.clickTest = function() {
        alert('Example of infowindow with ng-click')
    };
    
    initialize();
    
    
    var navigation, target, options;

    function success(pos) {
        var crd = pos.coords;
        geoChange(crd.latitude,crd.longitude);
        
        if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
            alert('Καλώς ήρθατε στο Dion Palace!');
            navigator.geolocation.clearWatch(navigation);
        }
    }

    function error(err) {
        alert('ERROR(' + err.code + '): ' + err.message);
    }

    target = {
        latitude : 40.1500366,
        longitude: 22.5463697
    };

    options = {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
    };

}])
