hotelApp.controller('ActivityController', ['$scope','$stateParams', function($scope, $stateParams) {
	
    var activitties = [
        {
            'id':'1',
            'type':'Spa',
            'thump':'thm-spa',
            'header':'Spa σάουνα χαμάμ',
            'images':['big-spa'],
            'text':'Spa area με εσωτερική θερμαινόμενη πισίνα, γυμναστήριο, χαμάμ, σάουνα',
            'link':'',
            'phone':'2352061431',
            'page':''      
        },
        {
            'id':'2',
            'type':'Pool',
            'thump':'thm-pool',
            'header':'3 πισίνες',
            'images':['big-pool'],
            'text':'3 πισίνες εξωτερικές (η μία 25Χ25 τ.μ) και μία παιδική',
            'link':'',
            'phone':'',
            'page':''
        },
        {
            'id':'3',
            'type':'Παιδική χαρά',
            'thump':'thm-playground',
            'header':'Δωρεάν ασύρματη πρόσβαση στο Internet',
            'images':['big-playground'],
            'text':'Παιδική χαρά υπαίθρια με σύγχρονα παιχνίδια',
            'link':'',
            'phone':'',
            'page':''
        },
        {
            'id':'4',
            'type':'Τένις',
            'thump':'thm-tenis',
            'header':'2 γήπεδα tennis',
            'images':['big-tenis'],
            'text':'Το ξενοδοχείο διαθέτει δύο γήπεδα τένις, στις αθλητικές του εγκαταστάσεις',
            'link':'',
            'phone':'',
            'page':''
        },
        {
            'id':'5',
            'type':'Ping Pong',
            'thump':'thm-table-tennis',
            'header':'Table Tennis',
            'images':['big-table-tennis'],
            'text':'Στο ξενοδοχείο υπάρχουν τραπέζια για πινκ πονκ',
            'link':'',
            'phone':'',
            'page':''
        },
        {
            'id':'6',
            'type':'Water Polo',
            'thump':'thm-water-polo',
            'header':'Water Polo Facilities',
            'images':['big-water-polo'],
            'text':'Στις πισίνες μας έχουμε εγκαταστάσεις για water polo',
            'link':'',
            'phone':'',
            'page':''
        }
    ];        
    
    $scope.activityData;
    var activity;
    var loop = 0;

    for(activity in activitties){
        if($stateParams.activityId === activitties[loop].id){

            $scope.activityData = activitties[loop];
            break;
        }
        loop++;
    }

}]);


