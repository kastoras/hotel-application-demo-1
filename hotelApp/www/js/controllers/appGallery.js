hotelApp.controller('GalleryController', ['$scope', function($scope) {
    $scope.galleryData = [
        {
            'header':'Πανοραμική θέα',
            'image':'panoramic',
            'text':'Οι εγκαταστάσεις του Δίον από αεροφωτογραφία',            
            'page':''      
        },
        {
            'header':'Spa',
            'image':'spa',
            'text':'Spa area με εσωτερική θερμαινόμενη πισίνα, γυμναστήριο, χαμάμ, σάουνα',            
            'page':'app.lespa'      
        },
        {
            'header':'By night',
            'image':'by-night1',
            'text':'',            
            'page':''      
        },
        {
            'header':'Παραλία',
            'image':'beach',
            'text':'Το ξενοδοχείο βρίσκετε δίπλα στη θάλασσα',            
            'page':''      
        },
        {
            'header':'Πισίνα',
            'image':'dion-outside-pool',
            'text':'Το ξενοδοχεία διαθέτει 3 εξωτερικές πισίνες',            
            'page':''      
        },
        {
            'header':'Restaurants',
            'image':'lunch',
            'text':'Επισκεφθείτε τα εστιατόρια μας, για το γεύμα σας',            
            'page':'app.restaurants'      
        },
        {
            'header':'Dion Lights',
            'image':'by-night2',
            'text':'',            
            'page':''      
        },
        {
            'header':'Εσωτερική πισίνα',
            'image':'inside-pool',
            'text':'Το Δίον διαθέτει και εσωτερική πισίνα',            
            'page':''      
        },
        {
            'header':'Dion Palace',
            'image':'logo',
            'text':'',            
            'page':'app.home'      
        }
    ];
}]);