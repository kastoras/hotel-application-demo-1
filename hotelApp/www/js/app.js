// Ionic Starter App

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var hotelApp = angular.module('your_app_name', ['ionic', 'angularMoment','underscore', 'ngMap', 'ngResource', 'ngCordova', 'slugifier', 'ionic.contrib.ui.tinderCards']);

hotelApp.run(['$ionicPlatform', function($ionicPlatform) {

  $ionicPlatform.on("deviceready", function(){
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    //PushNotificationsService.register();
  });

  /*$ionicPlatform.on("resume", function(){
    PushNotificationsService.register();
  });*/
}])


.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider

  //INTRO
  /*.state('walkthrough', {
    url: "/",
    templateUrl: "walkthrough.html",
    controller: 'WalkthroughCtrl'
  })*/

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/side-menu.html",
    controller: 'AppMenuController'
  })

  .state('app.maps', {
    url: "/maps",
    views: {
      'menuContent': {
        templateUrl: "templates/app-maps.html",
        controller: 'MapsController'
      }
    }
  })

  //LAYOUTS
  .state('app.restaurants', {
    url: "/restaurants",
    views: {
      'menuContent': {
        templateUrl: "templates/app-restaurantsBars.html",
        controller: 'RestaurantsBarsController'
      }
    }
  })
  
  .state('app.restaurant', {
    url: "/restaurant/:restaurantId",
    views: {
      'menuContent': {
        templateUrl: "templates/app-restaurantBar.html",
        controller: 'RestaurantBarController'
      }
    }
  })

  .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/app-home.html",
        controller: 'HomeController'
      }
    }
  })

  .state('app.rooms', {
    url: "/rooms",
    views: {
      'menuContent': {
        templateUrl: "templates/app-rooms.html",
        controller: 'RoomsController'
      }
    }
  })
  
  .state('app.room', {
    url: "/room/:roomId",
    views: {
      'menuContent': {
        templateUrl: "templates/app-room.html",
        controller: 'RoomController'
      }
    }
  })

  .state('app.services', {
    url: "/servicies",
    views: {
      'menuContent': {
        templateUrl: "templates/app-services.html",
        controller: 'ServiciesController'
      }
    }
  })
  
  .state('app.service', {
    url: "/service/:serviceId",
    views: {
      'menuContent': {
        templateUrl: "templates/app-service.html",
        controller: 'ServiceController'
      }
    }
  })
  
  .state('app.activities', {
    url: "/activities",
    views: {
      'menuContent': {
        templateUrl: "templates/app-activities.html",
        controller: 'ActivitiesController'
      }
    }
  })  
  
  .state('app.activity', {
    url: "/activity/:activityId",
    views: {
      'menuContent': {
        templateUrl: "templates/app-activity.html",
        controller: 'ActivityController'
      }
    }
  })
  
  .state('app.gallery', {
    url: "/gallery",
    views: {
      'menuContent': {
        templateUrl: "templates/app-gallery.html",
        controller: 'GalleryController'
      }
    }
  })
  
  .state('app.contact', {
    url: "/contact",
    views: {
      'menuContent': {
        templateUrl: "templates/app-contact.html",
        controller: 'ContactController'
      }
    }
  })
  
  .state('app.lespa', {
    url: "/lespa",
    views: {
      'menuContent': {
        templateUrl: "templates/app-lespa.html"
      }
    }
  })  
  
  .state('app.informations', {
    url: "/informations",
    views: {
      'menuContent': {
        templateUrl: "templates/app-informations.html"
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
}]);
