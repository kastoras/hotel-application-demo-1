hotelApp.controller('ServiceController', ['$scope','$stateParams', function($scope, $stateParams) {
	
        var posts = [
            {
                'id':'1',
                'service':'Reception',
                'thump':'thm-reception',
                'header':'Room service / Reception',
                'images':['big-reception'],
                'text':'Room service και 24ωρη υποδοχή στο ξενοδοχείο',
                'link':'',
                'phone':'2352061431',
                'page':''      
            },
            {
                'id':'2',
                'service':'Parking',
                'thump':'thm-parking',
                'header':'2 parking με κάμερες ασφαλείας',
                'images':['big-parking'],
                'text':'Δύο parking με πολύ χώρο και με κάμερες ασφαλείας για την προστασίας σας',
                'link':'',
                'phone':'',
                'page':''
            },
            {
                'id':'3',
                'service':'Wi Fi',
                'thump':'thm-wifi',
                'header':'Δωρεάν ασύρματη πρόσβαση στο Internet',
                'images':['big-wifi'],
                'text':'Δωρεάν ασύρματη πρόσβαση στο Internet σε όλους τους χώρους του ξενοδοχείου',
                'link':'',
                'phone':'',
                'page':''
            },
            {
                'id':'4',
                'service':'Εστιατόρια | Bar',
                'thump':'thm-restaurant',
                'header':'2 εστιατόρια και 4 μπαρ',
                'images':['big-restaurant'],
                'text':'Το ξενοδοχείου διαθέτει δύο εστιατόρια και και τέσσερα μπαρ για όλες τις προτιμήσεις',
                'link':'online-reserve',
                'phone':'23520.61431',
                'page':'app.restaurants'
            },
            {
                'id':'5',
                'service':'Συνεδριακός Χώρος',
                'thump':'thm-bussines',
                'header':'2 Συνεδριακούς χώρους',
                'images':['big-bussines'],
                'text':'Στο ξενοδοχείο υπάρχουν δυο συνεδριακοί χώροι.',
                'link':'',
                'phone':'',
                'page':''
            },
            {
                'id':'6',
                'service':'Beach',
                'thump':'thm-beach',
                'header':'Ξαπλώστρες και ομπρέλες στην παραλία και στις πισίνες δωρεάν',
                'images':['big-beach'],
                'text':'Ξαπλώστρες και ομπρέλες στην παραλία και στις πισίνες δωρεάν',
                'link':'',
                'phone':'',
                'page':''
            }  
        ];
        $scope.serviceData;
        var post;
        var loop = 0;
        
        for(post in posts){
            if($stateParams.serviceId === posts[loop].id){
                $scope.serviceData = posts[loop];
                break;
            }
            loop++;
        }
        
        //console.log($scope.posts[0].id);
}]);