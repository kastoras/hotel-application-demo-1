hotelApp.controller('RestaurantsBarsController', ['$scope', function($scope) {
    
        $scope.restaurantsBarsData = [
        {
            'id':'1',
            'name':'Μύτικας',
            'description':'Κεντρικό εστιατόριο',
            'icon':'ion-android-restaurant',
            'images':['main'],
            'text':'Προσφέρεται πρωινό σε πλούσιο μπουφέ ελληνικού και αμερικανικού τύπου',
            'link':'',
            'phone':'2352061431',
            'breakfast':'07.30 – 10.00',
            'lunch':'13.00 – 15.00',
            'enening':'19.00 – 21.30',
            'open':''            
        },
        {
            'id':'2',
            'name':'Le Chandelier',
            'description':'Εστιατόριο a la carte',
            'icon':'ion-android-restaurant',
            'images':['le'],
            'text':'Σε πολύ κομψή και ρομαντική διακόσμηση και μια ατμόσφαιρα υπό το φως των κεριών παρέχει επιλογές πιάτων Gourmet κουζίνας.',
            'link':'',
            'phone':'2352061431',
            'breakfast':'',
            'lunch':'',
            'enening':'',
            'open':''     
        },
        {
            'id':'3',
            'name':'La Medousa',
            'description':'Ταβέρνα',
            'icon':'ion-fork',
            'images':['medusa'],
            'text':'Είναι ανοικτή κατά τη θερινή περίοδο, δίπλα στις πισίνες σε μια μαγευτική ατμόσφαιρα. Λειτουργεί a la carte με επιλογές Μεσογειακής κουζίνας και specialdinners. Εδώ οι γευστικές επιλογές και το σέρβις ικανοποιούν ακόμα και τον ποιο απαιτητικό πελάτη. Ανοιχτά από τις 13.00 το μεσημέρι.',
            'link':'',
            'phone':'2352061431',
            'breakfast':'',
            'lunch':'',
            'enening':'',
            'open':'Ater 13.00'
        },
        {
            'id':'4',
            'name':'Mousses',
            'description':'Main bar',
            'icon':'ion-android-bar',
            'images':['beach-bar'],
            'text':'Βρίσκεται στον χώρο του Lobby (απαλή μουσική και ποικιλία από ποτά, κοκτέιλ και σνακς).',
            'link':'',
            'phone':'2352061431',
            'breakfast':'',
            'lunch':'',
            'enening':'',
            'open':'17.00 – 23.00'
        },
        {
            'id':'5',
            'name':'Beach Bar',
            'description':'Το Beach Bar του ξενοδοχείου',
            'icon':'ion-help-buoy',
            'images':['beach-bar'],
            'text':'Βρίσκεται μπροστά στην θάλασσα, κοντά στις πισίνες. Τα βράδια διοργανώνονται πάρτι, με DJ.',
            'link':'',
            'phone':'2352061431',
            'breakfast':'',
            'lunch':'',
            'enening':'',
            'open':'9.00 – 23.00'
        },
        {
            'id':'6',
            'name':'Sushi Bar',
            'description':'',
            'icon':'ion-android-bar',
            'images':['sushi-bar'],
            'text':'',
            'link':'',
            'phone':'2352061431',
            'breakfast':'',
            'lunch':'',
            'enening':'',
            'open':'Μετά 20.30'
        }
    ];
}])