// TINDER CARDS
hotelApp.controller('HomeController', ['$scope', function($scope) {

	$scope.cards = [];
        $scope.siler = 0;

	$scope.addCard = function(img, name) {
		var newCard = {image: img, name: name};
		newCard.id = Math.random();
		$scope.cards.unshift(angular.extend({}, newCard));
	};

	$scope.addFirstCards = function() {
                $scope.addCard("img/homepage/home-page-1.jpg","Για μπάνιο ή χαλάρωση στις πισίνες");
		$scope.addCard("img/homepage/home-page-2.jpg","Εξοπλισμένο πλήρως, με υψηλή αισθητική");
		$scope.addCard("img/homepage/home-page-3.jpg","Πάνω στη θάλασσα, κάτω από τον Όλυμπο");
		$scope.addCard("img/homepage/home-page-4.jpg","Πολυτελής Διαμονή - Αξέχαστες Διακοπές");
	};

	$scope.addFirstCards();

	$scope.cardDestroyed = function(index) {
		$scope.cards.splice(index, 1);
                
                if($scope.siler === 0){
                    $scope.addCard("img/homepage/home-page-4.jpg","Πολυτελής Διαμονή - Αξέχαστες Διακοπές");
                }
                if($scope.siler === 1){
                    $scope.addCard("img/homepage/home-page-3.jpg","Πάνω στη θάλασσα, κάτω από τον Όλυμπο");
                }
                if($scope.siler === 2){
                    $scope.addCard("img/homepage/home-page-2.jpg","Εξοπλισμένο πλήρως, με υψηλή αισθητική");
                }
                if($scope.siler === 3){
                    $scope.addCard("img/homepage/home-page-1.jpg","Για μπάνιο ή χαλάρωση στις πισίνες");
                    $scope.siler = 0;
                }              
                $scope.siler++;
	};
}]);