hotelApp.controller('RoomsController', ['$scope', function($scope) {
    
    $scope.roomsData = [
        {
            'id':'classic-double-room',
            'name':'Classic Double Room',
            'thump':'img/rooms/classic-double-room.jpg',
            'header':'',
            'images':[''],
            'text':''
        },
        {
            'id':'superior-sea-view',
            'name':'Superior Sea View',
            'thump':'img/rooms/superior-sea.jpg',
            'header':'',
            'images':[''],
            'text':''
        },
        {
            'id':'classic-mountain-garden-view',
            'name':'Classic Mountain/Garden View',
            'thump':'img/rooms/classic-mountain.jpg',
            'header':'',
            'images':[''],
            'text':''
        },
        {
            'id':'seafront -bungalows',
            'name':'Seafront Bungalows',
            'thump':'img/rooms/seafront-bungalow.jpg',
            'header':'',
            'images':[''],
            'text':''
        },
        {
            'id':'junior-suite-with-private-pool',
            'name':'Junior Suite With Private Pool',
            'thump':'img/rooms/junior-suite.jpg',
            'header':'',
            'images':[''],
            'text':''
        },
        {
            'id':'presidential-suite',
            'name':'Presidential Suite',
            'thump':'img/rooms/presidential-suite.jpg',
            'header':'',
            'images':[''],
            'text':''
        }
        
    ];           

}]);